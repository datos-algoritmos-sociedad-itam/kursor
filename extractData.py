from DBConnection import Connection
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import re
import config as conf

def extract_data(table_dict, row_name):
    """
        Description
        This function get the list of values inside the dictionary given a row name.        

        Parameters:
        table_dict - Dictionary of the table. The key is the row name and the value is a list of values
        row_name - Our target row

        Return:
        There are 3 posibilities
        1) Exact match for row_name and the text in the table
        2) The text in the table row contains row_name
        3) Return None because we didn't found the row or anything similar
        
    """
    if len(table_dict) == 0:
        return None, None, None
    target = row_name.upper().strip()
    target_regex = ""
    target_match = ""
    target_key = ""

    for key in table_dict.keys():
        if target == key:
            target_match = key 
            break
        elif re.search(target, key, re.IGNORECASE):
            # True if the row name contains what I want
            target_regex = key     

    if target_match == "" and target_regex == "":        
        return None, None, None
    elif target_match != "":
        # Priority to exact match
        target_key = target_match
    else:
        # target_regex != ""
        target_key = target_regex
    
    
    row = table_dict[target_key]

    previous_value = None
    try:
        previous_value = round(float(row[3]),2)
    except:
        previous_value = None

    current_value = None
    try:
        current_value = round(float(row[4]),2)
    except:
        current_value = None
    
    rate = None
    if current_value is not None and previous_value is not None:
        if (current_value < 0 and previous_value > 0) or (current_value > 0 and previous_value < 0):
            rate = "N/A"
        else:
            rate = float(row[5])

    return previous_value, current_value, rate


def getTableValues(driver):
    """
        Description
        This function extracts the data from the table and save it in a dictionary where the key
        is the row name and the value is a list of all the values.
        Check a sample table here http://presto.hacienda.gob.mx/presto/files/system/mashlets/app_layout_estopor/index.html?param_formato_desc=Ingresos%20Presupuestarios%20del%20Sector%20P%C3%BAblico&param_formato=3&param_unidad=1&param_tipo=1&param_lenguaje=1&param_clasificacion=1&

        Parameters:
        driver - Driver for current web page

        Return:
        A dictionary with the data from the table.
        
    """
    
    table_dict = {}
    
    try:        
        html = driver.page_source
        soup = BeautifulSoup(html, features="lxml")        
        table = soup.find("table", attrs={"border":"0", "cellpadding":"0", "cellspacing":"0", "class":"x-grid-table x-grid-table-resizer"})                
        for tr in table.tbody.findAll("tr"):            
            flag = False           
            title = ""
            values = []
            for td in tr.findAll("td"):            
                if not flag:
                    title =  str(td.getText()).upper().strip()
                    flag =  True         
                else:
                    values.append(td.getText().strip().replace(',', ''))
            if table_dict.get(title, None) is None:
                # It just adds the first appearance
                table_dict[title] = values        
    except:
        table_dict = {}
        
    return table_dict


def getDate(driver):
    """
        Description
        This function extracts the date from the page.
        Check http://presto.hacienda.gob.mx/presto/files/system/mashlets/app_layout_estopor/index.html?param_formato_desc=Ingresos%20Presupuestarios%20del%20Sector%20P%C3%BAblico&param_formato=3&param_unidad=1&param_tipo=1&param_lenguaje=1&param_clasificacion=1&

        Parameters:
        driver - Driver for current web page

        Return:
        - Previous month and year
        - Current month and year
    """
    prev_month = ""
    cur_month = ""           
    prev_year = "" 
    cur_year = ""
    try:        
        html = driver.page_source
        soup = BeautifulSoup(html, features="lxml")
        regex = "(Enero)((\s|-Febrero|-Marzo|-Abril|-Mayo|-Junio|-Agosto|-Septiembre|-Octubre|-Noviembre|-Diciembre)+)(\s*)(\([0-9]{4}-[0-9]{4}\))"
        # match de Enero-UltimoMes (Año anterior - Año actual), ej Enero-Febrero (2020-2021) o Enero (2020-2021)
        m = re.search(regex, str(soup))
        date = m.group(0).replace(")", "").replace("(", "")
        date = date.split(" ")
        months = date[0].split("-")
        prev_month = "Enero"
        cur_month = ""
        if len(months) != 1:
            cur_month = months[1]            
        prev_year, cur_year = date[1].split("-")        
    except:
        prev_month = ""
        cur_month = ""           
        prev_year = "" 
        cur_year = ""

    return prev_month, cur_month, prev_year, cur_year

def extractInfo():
    linkIngPub = 'http://presto.hacienda.gob.mx/presto/files/system/mashlets/app_layout_estopor/index.html?param_formato_desc=Ingresos%20Presupuestarios%20del%20Sector%20P%C3%BAblico&param_formato=3&param_unidad=1&param_tipo=1&param_lenguaje=1&param_clasificacion=1&'
    print("# OF ATTEMPTS: " + conf.MIN_ATTEMPTS, "")
    for i in range(int(conf.MIN_ATTEMPTS)):
        print("Trying attempt #"+str(i+1))
        success = 0
        driver = None
        try:
            myConn = Connection()    
            idIngPub = int(myConn.executeSelect("select idReport from schemainfographics.tbReport WHERE report like 'Ingresos presupuestarios del sector p%blico' LIMIT 1;")[0][0])
            
            options = webdriver.ChromeOptions()   
            options.add_argument("--headless") # Disable window from being pop up  
            options.add_argument("--log-level=3") # Disable unnecessary logs, just FATAL LOGS
            driver = webdriver.Chrome(options=options)       
            driver.get(linkIngPub)

            # Le damos 1 minuto = 60s para que cargue el contenido generado por el JS
            table = WebDriverWait(driver, 60).until(        
                EC.presence_of_element_located((By.XPATH, '//*[@id="gridview-1051"]/table'))        
            )

            dictIng = getTableValues(driver)    
            prevMonth, curMonth, prevYear, curYear =  getDate(driver)
            months = prevMonth+"-"+ curMonth
            years = prevYear + "-" + curYear
            
            idMonths = int(myConn.executeSelect("select idMonths from schemainfographics.tbMonths WHERE months = '" + months +"' LIMIT 1;")[0][0])    
            yearsRecord = myConn.executeSelect("select idPeriod from schemainfographics.tbYears WHERE periodo = '" + years +"' LIMIT 1;")
            idYears = -1
            if len(yearsRecord) == 0:
                # Insert new period
                sql_string = "INSERT INTO SchemaInfographics.tbYears(periodo)"
                idYears = myConn.makeInsertAndGetID(sql_string, [(years,)], "idPeriod")        
                #print(idYears)
            else:
                #print("Period already exists")
                idYears = int(myConn.executeSelect("SELECT idPeriod FROM SchemaInfographics.tbYears WHERE periodo = '" + years + "' LIMIT 1;")[0][0])

            record = myConn.executeSelect("SELECT * FROM SchemaInfographics.tbValues WHERE idperiod = '" + str(idYears) + "' and idmonths = '" + str(idMonths) + "' LIMIT 1;")
            
            if len(record) == 0:
                report = "Ingresos presupuestarios %"
                idReport = myConn.executeSelect("SELECT idReport FROM SchemaInfographics.tbReport WHERE report like '" + report + "' LIMIT 1;")[0][0]
                categories = ["Petroleros", "Pemex", "Tributarios", "Impuesto sobre la renta", 
                                "Impuesto al valor agregado", "IEPS gasolinas y diesel",
                                "IEPS distinto de gasolinas y diesel",
                                "No tributarios", "IMSS", "ISSSTE", "CFE"]
                            
                values = []    
                for cat in categories:            
                    acumulado_prev, acumulado_act, acumulado_tasa = extract_data(dictIng, cat)            
                    idCat = myConn.executeSelect("SELECT idCategory FROM SchemaInfographics.tbCategory WHERE category like '" + cat + "' LIMIT 1;")[0][0]                            
                    values.append((idYears, idMonths, idCat , acumulado_prev, acumulado_act, acumulado_tasa, idReport, ))        

                myConn.makeInserts("INSERT INTO SchemaInfographics.tbValues(idPeriod, idMonths, idCategory, previous, current, rate, idReport)", values ) 
                
                
                values = []
                # Otros impuestos              
                idOtros = myConn.executeSelect("SELECT idCategory FROM SchemaInfographics.tbCategory WHERE category like 'Otros impuestos' LIMIT 1;")[0][0]
                suma = myConn.executeSelect(f"SELECT SUM(previous), SUM(current) FROM SchemaInfographics.tbValues WHERE idCategory IN (SELECT idCategory FROM SchemaInfographics.tbCategory WHERE partof like 'Tributarios') AND idPeriod = {idYears} AND idMonths = {idMonths};")[0]
                pre =  round(float(suma[0]),2)
                cur = round(float(suma[1]),2)

                trib = myConn.executeSelect(f"SELECT previous, current FROM SchemaInfographics.tbValues WHERE idCategory IN (SELECT idCategory FROM SchemaInfographics.tbCategory WHERE category like 'Tributarios') AND idPeriod = {idYears} AND idMonths = {idMonths};")[0]
                tribPre =  round(float(trib[0]),2)
                tribCur = round(float(trib[1]),2)                                

                values.append((idYears, idMonths, idOtros, round(tribPre-pre,2), round(tribCur-cur,2), None, idReport, ))                        
                
                # FMP = Petroleros - Pemex

                idPemex = myConn.executeSelect("SELECT idCategory FROM SchemaInfographics.tbCategory WHERE category like 'FMP' LIMIT 1;")[0][0]
                suma = myConn.executeSelect(f"SELECT previous, current FROM SchemaInfographics.tbValues WHERE idCategory IN (SELECT idCategory FROM SchemaInfographics.tbCategory WHERE partof like 'Petroleros') AND idPeriod = {idYears} AND idMonths = {idMonths};")[0]
                pre =  round(float(suma[0]),2)
                cur = round(float(suma[1]),2)

                pet = myConn.executeSelect(f"SELECT previous, current FROM SchemaInfographics.tbValues WHERE idCategory IN (SELECT idCategory FROM SchemaInfographics.tbCategory WHERE category like 'Petroleros') AND idPeriod = {idYears} AND idMonths = {idMonths};")[0]
                petPre =  round(float(pet[0]),2)
                petCur = round(float(pet[1]),2)
                                
                values.append((idYears, idMonths, idPemex, round(petPre-pre,2), round(petCur-cur,2), None, idReport, ))        

                myConn.makeInserts("INSERT INTO SchemaInfographics.tbValues(idPeriod, idMonths, idCategory, previous, current, rate, idReport)", values )
                      

            else:
                print("Information from " + years + " " + months  +" was already retrieved in the database")
            success = 1
        except Exception as error:
            print("Attempt #"+str(i)+" failed")
            print("Error" + str(error))

        finally:
            if driver is not None:
                driver.quit()
            myConn = None
            if success == 1:
                break

    if success == 1:
        print("Script executed succesfully")
    else:
        print("Script execution failed")
    return success

def lambda_handler(event, context):
    extractInfo()

# extractInfo() #Uncomment to test locally running extractData.py
