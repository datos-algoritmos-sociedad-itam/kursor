from enum import Flag
import pandas as pd
import dash
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import  Output, Input
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from DBConnection import Connection


pd.set_option('display.max_columns', None)
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])

colors = ["#000000", "#803E75","#FF6800","#A6BDD7","#C10020","#007D34", "#CEA262",
    "#817066","#F6768E", "#00538A", "#FF7A5C", "#53377A", 
    "#FF8E00","#B32851", "#F4C800", "#7F180D", "#93AA00", "#593315", "#F13A13", "#232C16",
    "#ee4035",  "#f37736","#fdf498", "#7bc043", "#0392cf", "#4a4e4d", "#0e9aa7", "#3da4ab" , "#f6cd61",  "#fe8a71" ]

# Query DB
conn = Connection()
column_names = ["Years", "Months", "Name", "Part of", "Previous", "Current", "Rate" ]
tuples = conn.executeSelect("SELECT tby.periodo, tbm.months, tbc.category, tbc.partOf, tbv.previous, tbv.current, tbv.rate FROM SchemaInfographics.tbValues AS tbv, SchemaInfographics.tbYears AS tby, SchemaInfographics.tbMonths AS tbm, SchemaInfographics.tbCategory AS tbc WHERE tbv.idPeriod = tby.idPeriod AND tbv.idMonths = tbm.idMonths AND tbv.idCategory = tbc.idCategory;")

#Setup dataframe
ing_pub_data = pd.DataFrame(tuples, columns=column_names)
ing_pub_data["Previous"] = ing_pub_data["Previous"].astype(float)
ing_pub_data["Current"] = ing_pub_data["Current"].astype(float)
ing_pub_data["Rate"] = ing_pub_data["Rate"].astype(float)


dfDate = ing_pub_data[["Years", "Months"]]
ops = [rowDF["Years"] + " " +rowDF["Months"] for ind, rowDF in dfDate.drop_duplicates().iterrows()]


app.layout = html.Div([
    html.Img(src='https://ciep.mx/wp-content/uploads/2020/06/ciep-logo1.png'),
    html.H3('Centro de Investigación Económica y Presupuestaria'),       
    html.Br(),
    dbc.Tabs([
       dbc.Tab([
           html.Br(),
           html.P('Seleccionar opción de comparación'),
           dcc.Dropdown(id='date_dropdown',
                 value=max(ops),
                 options=[{'label': option, 'value': option}
                          for option in ops]),                    
            dcc.Graph(id='ingresos_publicos_chart')
       ], label='Ingresos públicos'),
        dbc.Tab([
            html.Ul([
                html.Br(),
                html.Li('Book title: Interactive Dashboards and Data Apps with Plotly and Dash'),
                html.Li(['GitHub repo: ',
                         html.A('https://github.com/PacktPublishing/Interactive-Dashboards-and-Data-Apps-with-Plotly-and-Dash',
                                href='https://github.com/PacktPublishing/Interactive-Dashboards-and-Data-Apps-with-Plotly-and-Dash')
                         ])
            ])
        ], label='Proyección de ingresos públicos'),
        dbc.Tab([
            html.Ul([
                html.Br(),
                html.Li('Book title: Interactive Dashboards and Data Apps with Plotly and Dash'),
                html.Li(['GitHub repo: ',
                         html.A('https://github.com/PacktPublishing/Interactive-Dashboards-and-Data-Apps-with-Plotly-and-Dash',
                                href='https://github.com/PacktPublishing/Interactive-Dashboards-and-Data-Apps-with-Plotly-and-Dash')
                         ])
            ])
        ], label='Gasto programable'),
        dbc.Tab([
            html.Ul([
                html.Br(),
                html.Li('Book title: Interactive Dashboards and Data Apps with Plotly and Dash'),
                html.Li(['GitHub repo: ',
                         html.A('https://github.com/PacktPublishing/Interactive-Dashboards-and-Data-Apps-with-Plotly-and-Dash',
                                href='https://github.com/PacktPublishing/Interactive-Dashboards-and-Data-Apps-with-Plotly-and-Dash')
                         ])
            ])
        ], label='Programas prioritarios'),
        dbc.Tab([
            html.Ul([
                html.Br(),
                html.Li('Book title: Interactive Dashboards and Data Apps with Plotly and Dash'),
                html.Li(['GitHub repo: ',
                         html.A('https://github.com/PacktPublishing/Interactive-Dashboards-and-Data-Apps-with-Plotly-and-Dash',
                                href='https://github.com/PacktPublishing/Interactive-Dashboards-and-Data-Apps-with-Plotly-and-Dash')
                         ])
            ])
        ], label='Reporte de deuda'),
        dbc.Tab([
            html.Ul([
                html.Br(),
                html.Li('Book title: Interactive Dashboards and Data Apps with Plotly and Dash'),
                html.Li(['GitHub repo: ',
                         html.A('https://github.com/PacktPublishing/Interactive-Dashboards-and-Data-Apps-with-Plotly-and-Dash',
                                href='https://github.com/PacktPublishing/Interactive-Dashboards-and-Data-Apps-with-Plotly-and-Dash')
                         ])
            ])
        ], label='Gasto neto total'),
        dbc.Tab([
            html.Ul([
                html.Br(),
                html.Li('Book title: Interactive Dashboards and Data Apps with Plotly and Dash'),
                html.Li(['GitHub repo: ',
                         html.A('https://github.com/PacktPublishing/Interactive-Dashboards-and-Data-Apps-with-Plotly-and-Dash',
                                href='https://github.com/PacktPublishing/Interactive-Dashboards-and-Data-Apps-with-Plotly-and-Dash')
                         ])
            ])
        ], label='Finanzas públicas')
    ])

])


@app.callback(Output('ingresos_publicos_chart', 'figure'),
              Input('date_dropdown', 'value'))

def plot_ingresos_publicos(years_months):    

    years = years_months.split(" ")[0]
    months = years_months.split(" ")[1]
    fig  = make_subplots()
    fig.update_layout(  title_text  = f'INGRESOS PÚBLICOS EN MILLONES DE PESOS. {months} {years}', 
                        xaxis = {'visible':False}, yaxis = {'visible':False},
                        height = 700, showlegend=False)
    

    by_year_month = ing_pub_data[ (ing_pub_data["Years"] == years) & (ing_pub_data["Months"] == months)]        
    parts = by_year_month[ by_year_month["Part of"] != ""]
    totals = by_year_month[ by_year_month["Part of"] == ""].sort_values(by = ["Previous"])        
    n = len(totals)

    # previous--------------------------------------
    totals["xpre"] = [i*.5 for i in range(1, n+1)]
    totals["ypre"] = [i if i < 2*(n-1) else i+3 for i in range(1, 2*n+1, 2)]
    
    custom_text = [ ]    
    totalPre = 0.0
    for ind, row in totals.iterrows():
        curdf = parts[parts["Part of"] == row["Name"]][["Name", "Previous"]]
        s = ''
        if len(curdf) != 0:                        
            catTotal = 0.0
            for i,r in curdf.iterrows():
                s+= "<b>"+str(r["Name"])+"</b>" +" : " + str(r["Previous"]) + "<br>"
                catTotal += r["Previous"]
            s = '<b>'+str(row["Name"])+" : "+ str(catTotal)+"</b><br>" + s
            totalPre+= catTotal
        else:
            # Category by itself
            curdf = totals[totals["Name"] == row["Name"]][["Name", "Previous"]] 
            s = '<b>'+str(curdf["Name"].values[0])+"<br>"+ str(curdf["Previous"].values[0])+"</b><br>"
            totalPre += curdf["Previous"].values[0]

        custom_text.append(s)     
    totals["TextPre"]  = custom_text
    totals["SizePre"] = 1/totalPre*totals["Previous"]*100
    
    fig.add_scatter(x = totals["xpre"], y  = totals["ypre"],  mode = 'markers+text',marker_size = totals["SizePre"], 
                            marker_color = colors[:n],
                            text = totals["TextPre"] ,
                            hoverinfo='none',
                            name = "Previous"
                        )                        
    
    fig.update_traces(textposition='{} {}'.format('top', 'left'), selector=dict(name="Previous"))
    
    # Actuales -----------------------------------
    totals["xcur"] = [.5*2*n+1 - i*.5 for i in range(1, n+1)]
    totals["ycur"] = [i if i < 2*(n-1) else i+3 for i in range(1, 2*n+1, 2)]
    custom_text = [ ]    
    totalCur = 0.0
    for ind, row in totals.iterrows():
        curdf = parts[parts["Part of"] == row["Name"]][["Name", "Current"]]
        s = ''
        if len(curdf) != 0:                        
            catTotal = 0.0
            for i,r in curdf.iterrows():
                s+= "<b>"+str(r["Name"])+"</b>" +" : " + str(r["Current"]) + "<br>"
                catTotal += r["Current"]
            s = '<b>'+str(row["Name"])+" : "+ str(catTotal)+"</b><br>" + s
            totalCur+= catTotal
        else:
            curdf = totals[totals["Name"] == row["Name"]][["Name", "Current"]] 
            s = '<b>'+str(curdf["Name"].values[0])+"<br>"+ str(curdf["Current"].values[0])+"</b><br>"
            totalCur+= curdf["Current"].values[0]        

        custom_text.append(s)     
    totals["TextCur"]  = custom_text
    totals["SizeCur"] = 1/totalCur*totals["Current"]*100

    fig.add_scatter(x = totals["xcur"], y  = totals["ycur"],  mode = 'markers+text',marker_size = totals["SizeCur"], 
                            marker_color = colors[:n],
                            text = totals["TextCur"] ,
                            hoverinfo='none',
                            name = "Current"
                        )                        
    
    fig.update_traces(textposition='{} {}'.format('top', 'right'), selector=dict(name="Current"))
    
    # Flechas hacia el total anterior
    for ind, row in totals.iterrows():
        xs = [row["xpre"], 2]
        ys = [row["ypre"], 1]        
        fig.add_scatter(mode="lines", x=xs, y=ys, hoverinfo='none', line = dict(width=0.5, color = "#000000"))
    # Letrero total anterior
    fig.add_annotation(
        x=2,
        y=0,        
        text="<b>Total: "+str(totalPre)+"</b>",  
        showarrow=False,      
        font=dict(
            family="Arial",
            size=16,
            color="#000000"
        ),
        align="center",
        bgcolor="#b3ba00",
        borderwidth=2,
        borderpad=4,
        opacity=0.8
    )
    # Letrero fecha anterior
    fig.add_annotation(
        x=2,
        y= 22,        
        text="<b>"+ months.split("-")[1] +" " + years.split("-")[0] + "</b>",  
        showarrow=False,      
        font=dict(
            family="Arial",
            size=16,
            color="#000000"
        ),
        align="center",
        bgcolor="#b3ba00",
        borderwidth=2,
        borderpad=4,
        opacity=0.8
    )

    # Flechas hacia el total actual
    for ind, row in totals.iterrows():
        xs = [row["xcur"], 5]
        ys = [row["ycur"], 1]        
        fig.add_scatter(mode="lines", x=xs, y=ys, hoverinfo='none', line = dict(width=0.8, color = "#000000"))
    
    # Letrero de total actual
    fig.add_annotation(
        x=5,
        y=0,        
        text="<b>Total: "+str(totalCur)+"</b>",  
        showarrow=False,      
        font=dict(
            family="Arial",
            size=16,
            color="#000000"
        ),
        align="center",
        bgcolor="#b3ba00",
        borderwidth=2,
        borderpad=4,
        opacity=0.8
    )
    # Letrero fecha actual
    fig.add_annotation(
        x=5,
        y= 22,        
        text="<b>"+ months.split("-")[1] +" " + years.split("-")[1] + "</b>",  
        showarrow=False,      
        font=dict(
            family="Arial",
            size=16,
            color="#000000"
        ),
        align="center",
        bgcolor="#b3ba00",
        borderwidth=2,
        borderpad=4,
        opacity=0.8
    )

    # Rates --------------------
    # Líneas
    n_colors = len(colors) - 1 
    for ind, row in totals.iterrows():
        xs = [row["xcur"], row["xpre"]]
        ys = [row["ycur"], row["ypre"]]        
        fig.add_scatter(mode="lines", x=xs, y=ys, hoverinfo='none', line = dict(width=0.5, color = colors[n_colors]))
        n_colors-=1
        if(n_colors < 0):
            n_colors = len(colors)


    # Info
    n_colors = len(colors) - 1
    for ind, row in totals.iterrows():
        xs = [row["xcur"], row["xpre"]]
        ys = [row["ycur"], row["ypre"]]
        rate_val = str(row["Rate"])
        if rate_val != "N/A" and rate_val != "":
            rate_val += "%"

        fig.add_annotation(
            x= ( (xs[0]+xs[1])/2.0),
            y= ( (ys[0]+ys[1])/2.0),        
            text="<b> "+ rate_val+"</b>",  
            showarrow=False,      
            font=dict(
                family="Arial",
                size=16,
                color= colors[n_colors]
            ),
            align="center",
            borderwidth=2,
            bgcolor="#e5ecf6",
            bordercolor = colors[n_colors],
            borderpad=4            
        )
        n_colors-=1
        if(n_colors < 0):
            n_colors = len(colors)
    
    fig.add_annotation(
        x= (max(totals["xcur"]) + min(totals["xpre"]))/2.0,
        y= max(totals["ycur"]) + 2,        
        text="<b>"+ "Crecimiento real" + "</b>",  
        showarrow=False,      
        font=dict(
            family="Arial",
            size=16,
            color="grey"
        ),
        align="center",
        borderwidth=2,
        borderpad=4,
        opacity=0.8
    )


    return fig

if __name__ == '__main__':
    app.run_server(debug=False)

