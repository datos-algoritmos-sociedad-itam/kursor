import psycopg2
from config import config

class Connection:     
        
    def __getConnection(self):
        """ Connect to the PostgreSQL database server """
        conn = None
        try:
            # read connection parameters
            params = config()

            # connect to the PostgreSQL server            
            conn = psycopg2.connect(**params)
                        
        except (Exception, psycopg2.DatabaseError) as error:
            print("Error in DB: " + str(error))

        return conn

    def executeSelect(self, queryString):
        """ Executes the query """
        result = None
        conn = None
        try:
            conn = self.__getConnection()
            cur = conn.cursor()

            cur.execute(queryString)
            result = cur.fetchall()            

            cur.close()            
        except Exception as error:
            print("Error in DB: " + str(error))
        finally:
            if conn is not None:
                conn.close()
        return result
    
    def makeInserts(self, queryString, values):
        """ 
            Insert values given the queryString. 

            The query should follow the following structure:
            INSERT INTO schema_name.table_name(column_name_1, ... , column_name_n)          
        """        
        if len(values) == 0:
            return
        conn = None
        try:                                    
            conn = self.__getConnection()            
            cur = conn.cursor()
            

            n_columns = len(values[0])
            strs = "%s,"*(n_columns-1) + "%s"
            
            args = ','.join(cur.mogrify("("+ strs+")", i).decode('utf-8')
                for i in values)
            
            cur.execute(queryString +" VALUES " + (args))            


            conn.commit()
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print("Error in DB: " + str(error))
        finally:
            if conn is not None:
                conn.close()

    def makeInsertAndGetID(self, queryString, values, idName):
        """ 
            Insert values given the queryString. 

            The query should follow the following structure:
            INSERT INTO schema_name.table_name(column_name_1, ... , column_name_n)          
        """
        sql = queryString + " VALUES(%s) RETURNING " + idName        
        conn = None
        id = None
        try:
            
            conn = self.__getConnection()
            cur = conn.cursor()
            
            
            # execute the INSERT statement            
            cur.execute(sql, values)
            id = cur.fetchone()[0]

            
            conn.commit()            
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:            
            print("Error in DB: " + str(error))
        finally:
            if conn is not None:
                conn.close()
        return id
