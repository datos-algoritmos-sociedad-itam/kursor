/*
-- Run this for the first time in the SQL shell --

1) Create Database
sudo su postgres
psql -U postgres -c "CREATE DATABASE dbciep WITH ENCODING = 'UTF8' LC_COLLATE = 'es_ES.UTF-8' LC_CTYPE='es_ES.UTF-8' TEMPLATE=template0;"

2) psql postgres

3) Connect to the Database
\c dbciep

4) Execute the script 
\i path/to/ScriptDB.sql

*** Replace all "\" with "/" in windows

*/

-- Create Schema
CREATE SCHEMA IF NOT EXISTS SchemaInfographics;

-- For deleting tables
DROP TABLE SchemaInfographics.tbValues;
DROP TABLE SchemaInfographics.tbCategory;
DROP TABLE SchemaInfographics.tbMonths;
DROP TABLE SchemaInfographics.tbYears;
DROP TABLE SchemaInfographics.tbReport;




CREATE TABLE IF NOT EXISTS SchemaInfographics.tbReport(
    idReport INT PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY,
    report VARCHAR(200)
);

INSERT INTO SchemaInfographics.tbReport(report) VALUES('Ingresos presupuestarios del sector público');
INSERT INTO SchemaInfographics.tbReport(report) VALUES('Gasto programable');
INSERT INTO SchemaInfographics.tbReport(report) VALUES('Reporte de deuda');


CREATE TABLE IF NOT EXISTS SchemaInfographics.tbYears(
    idPeriod INT PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY,
    periodo VARCHAR(9)
);
INSERT INTO SchemaInfographics.tbYears(periodo) VALUES('2021-2022');
CREATE TABLE IF NOT EXISTS SchemaInfographics.tbMonths(
    idMonths INT PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY,
    months VARCHAR(25)
);

INSERT INTO SchemaInfographics.tbMonths(months) VALUES('Enero-');
INSERT INTO SchemaInfographics.tbMonths(months) VALUES('Enero-Febrero');
INSERT INTO SchemaInfographics.tbMonths(months) VALUES('Enero-Marzo');
INSERT INTO SchemaInfographics.tbMonths(months) VALUES('Enero-Abril');
INSERT INTO SchemaInfographics.tbMonths(months) VALUES('Enero-Mayo');
INSERT INTO SchemaInfographics.tbMonths(months) VALUES('Enero-Junio');
INSERT INTO SchemaInfographics.tbMonths(months) VALUES('Enero-Julio');
INSERT INTO SchemaInfographics.tbMonths(months) VALUES('Enero-Agosto');
INSERT INTO SchemaInfographics.tbMonths(months) VALUES('Enero-Septiembre');
INSERT INTO SchemaInfographics.tbMonths(months) VALUES('Enero-Octubre');
INSERT INTO SchemaInfographics.tbMonths(months) VALUES('Enero-Noviembre');
INSERT INTO SchemaInfographics.tbMonths(months) VALUES('Enero-Diciembre');


CREATE TABLE IF NOT EXISTS SchemaInfographics.tbCategory(
    idCategory INT PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY,
    category VARCHAR(75),
    partOf VARCHAR(75)
);

INSERT INTO SchemaInfographics.tbCategory(category, partOf) VALUES('Petroleros', '');
INSERT INTO SchemaInfographics.tbCategory(category, partOf) VALUES('Pemex', 'Petroleros');
INSERT INTO SchemaInfographics.tbCategory(category, partOf) VALUES('FMP', 'Petroleros');
INSERT INTO SchemaInfographics.tbCategory(category, partOf) VALUES('Tributarios', '');
INSERT INTO SchemaInfographics.tbCategory(category, partOf) VALUES('Impuesto sobre la renta', 'Tributarios');
INSERT INTO SchemaInfographics.tbCategory(category, partOf) VALUES('Impuesto al valor agregado', 'Tributarios');
INSERT INTO SchemaInfographics.tbCategory(category, partOf) VALUES('IEPS gasolinas y diesel', 'Tributarios');
INSERT INTO SchemaInfographics.tbCategory(category, partOf) VALUES('IEPS distinto de gasolinas y diesel', 'Tributarios');
INSERT INTO SchemaInfographics.tbCategory(category, partOf) VALUES('Otros impuestos', 'Tributarios');
INSERT INTO SchemaInfographics.tbCategory(category, partOf) VALUES('No tributarios', '');
INSERT INTO SchemaInfographics.tbCategory(category, partOf) VALUES('IMSS', '');
INSERT INTO SchemaInfographics.tbCategory(category, partOf) VALUES('ISSSTE', '');
INSERT INTO SchemaInfographics.tbCategory(category, partOf) VALUES('CFE', '');

CREATE TABLE IF NOT EXISTS SchemaInfographics.tbValues(
    idPeriod INT NOT NULL,
    idMonths INT NOT NULL,
    idCategory INT NOT NULL,
    previous NUMERIC,
    current NUMERIC,
    rate NUMERIC,
    idReport INT NOT NULL,
    PRIMARY KEY(idPeriod, idMonths, idCategory),
    FOREIGN KEY (idPeriod) REFERENCES SchemaInfographics.tbYears(idPeriod),
    FOREIGN KEY (idMonths) REFERENCES SchemaInfographics.tbMonths(idMonths),
    FOREIGN KEY (idCategory) REFERENCES SchemaInfographics.tbCategory(idCategory),
    FOREIGN KEY (idReport) REFERENCES SchemaInfographics.tbReport(idReport)
);

INSERT INTO SchemaInfographics.tbValues VALUES
    (1, 1, 1, 48080.8, 69569.3, 35.1, 1),
    (1, 1, 2, 13274.8, 66024.1, 364.5, 1), 
    (1, 1, 4, 355562.5, 379838.7, -0.2, 1),
    (1, 1, 5, 180432.1, 213533.4, 10.5, 1),
    (1, 1, 6, 115495.6, 112236.5, -9.2, 1),
    (1, 1, 7, 24183.6, 11315.8, -56.3, 1),
    (1, 1, 8, 26677.4, 30143.4, 5.5, 1),
    (1, 1, 10, 26970.4, 24280.9, -15.9, 1),
    (1, 1, 11, 32113.5, 37824.1, 10.0, 1),
    (1, 1, 12, 2574.7, 2418.4, -12.3, 1),
    (1, 1, 13, 27160.4, 29538.9, 1.6, 1),
    (1, 1, 9, 8773.8, 12609.6, NULL, 1),
    (1, 1, 3, 34806.0, 3545.2, NULL, 1);



-- Create user for the app

CREATE USER ciep WITH ENCRYPTED PASSWORD 'pa55w0r9';

GRANT ALL PRIVILEGES ON DATABASE dbciep TO ciep;
GRANT USAGE ON SCHEMA SchemaInfographics TO ciep;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA SchemaInfographics TO ciep;

