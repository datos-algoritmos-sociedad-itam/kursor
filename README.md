# Proyecto Kursor

Este proyecto tratará de automatizar los reportes trimestrales y mensuales de gastos generados por el Centro de Investigación Económica y Presupuestaria.

# Environment setup

Descargar Python 3.9, pues es el que se usará en el proyecto. 

Una vez que se tiene Python3.9, descargar el módulo `venv` desde la terminal, pues vamos a crear un ambiente virtual.
```
    pip install venv
```

### Preparar el ambiente de ejecución:


1. Crear el ambiente virtual:
``` 
python -m venv /path/to/venv
```

2. Activar el ambiente virtual:
```
/path/to/env/Scripts/activate.bat (Windows)
```

3. Instalar los requerimientos:
```
pip install -r requirements.txt
```

### Preparar la DB


Seguir los pasos del archivo `ScriptDB.sql`. Una vez que hayas tengas el servidor de postgres y las credenciales de la BD, hay que proveer los datos en el archivo `database.ini`, aunque los valores por default son para que corra en el localhost con el usuario creado por el `ScriptDB.sql`.

### Preparar el web scraper


Este paso solo es necesario para ejecutar el archivo `extractData.py`.

Solo necesitas descargar el driver de Google para hacer el web scraping [aquí](https://chromedriver.chromium.org/downloads). Checa qué versión es tu navegador Chrome [aquí](https://chromedriver.chromium.org/downloads/version-selection).


### Correr la app


Ya que el ambiente está listo, podemos correr la app, el script de la BD trae registros de una extracción para probar que funciona.
```
python -u "./app.py"
```

### Correr el scraper de forma local


Una vez que hiciste el paso `Preparar el web scraper`, puedes ejecutar el scraper corriendo el archivo `extractData.py`. Para ello, ve al archivo y descomenta la llamada a la función `extractInfo()`.
```
python -u "./extractData.py"
```

# Deployment

Necesitamos un servidor Linux(Ubuntu) conectado a internet, los archivos de la app y nuestra app.
Vamos a utilizar Gunicorn(el WSGI), nginx(el servidor web) y el código de nuestra app.


Loggueate como root y ejecuta los sig. comandos:

1. Actualizar paquetes en Linux
```
apt-get update
apt-get upgrade
```

2. Crear un usuario con menos privilegios
```
adduser dev-user
adduser dev-user sudo
exit
```

Ahora, conéctate via SSH con el  nuevo usuario.

Vamos a utilizar python 3.9.
Si al correr `python --version` nos dice "Command not found". Ejecutamos sudo apt install python3.9

Una vez que ya tenemos python, creamos el ambiente virtual.

1. Crear el ambiente virtual:
``` 
python -m venv /path/to/venv
```

2. Activar el ambiente virtual:
```
/path/to/env/Scripts/activate.bat (Windows)
```

3. Entrar al path del ambiente
```
cd /path/to/venv
```

4. Clonar el código del proyecto
```
git clone ...
```

5. Instalar los requerimientos:
```
pip install -r requirements.txt
```

6. Ejecutar la app para probar
```
python app_server.py
```

El código debería ejecutarse sin problemas.
Necesitamos la interface para que Flask trabaje con cualquier web server, esta interface es el WSGI.

## WSGI

Primero se prueba el WSGI.
1. Instalamos Gunicorn desde la terminal.
```
pip install gunicorn
```

2. Correr `gunicorn <app_module_name:server_name>` para activar el servidor WSGI.
El app_module_name es el nombre de nuestro archivo de python sin la extensión. El server_name es el nombre del servidor.
```
gunicorn app_server:server
```

Debería de correr sin problemas.

## nginx

Primero instalamos nginx. 

1. Instalar nginx
```
sudo apt install nginx
```

2. Creamos el archivo de configuración para nginx.

```
sudo nano /etc/nginx/sites-enabled/dash_app
```

Pegar el siguiente contenido, sustituyendo la IP del host:

```

server {

    listen 80;

    server_name <IP address>;

    location / {

        proxy_pass http://127.0.0.1:8000;

        proxy_set_header Host $host;

        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    }

  }

```

De esta forma hacemos a nginx un servidor proxy con el `proxy_pass` y le decimos que escuche al URL y al puerto que Gunicorn están escuchando.

3. Deslinquear la configuration por default.
```
sudo unlink /etc/nginx/sites-enabled/default
```

4. Recargar nginx
```
sudo nginx -s reload
```

## Correr la app

```
gunicorn app_server:server
```
# DB setup
1. In an EC2 instance run the following commands
    1. We need to first add the pgdg repository in Amazon Linux
    ```
    amazon-linux-extras install postgresql10
    ```

    2. Now, install postgresql 10
    ```
    yum install -y postgresql-server postgresql-devel
    /usr/bin/postgresql-setup –-initdb
    ```

    3. Start the postgres service
    ```
    systemctl enable postgresql
    systemctl start postgresql
    ```

    4. To check if the postgres service is running or not
    ```
    systemctl status postgresql
    ```
2. Seguir las instrucciones del script `ScriptBD.sql` para crear la BD.

# Extracción de datos

- Los datos de la bd se ponen en el archivo `database.ini`.
- El archivo que extra los datos es `extractData.py` por medio de la función `extractInfo()`.
Para ejecutar la función en una lambda function, seguir el siguiente tutorial para habilitar el chromedriver que se utiliza para el web scraping.
https://dev.to/awscommunity-asean/creating-an-api-that-runs-selenium-via-aws-lambda-3ck3

# Cosas pendientes
La extracción de los datos para las gráficas se puede ver en el notebook `web_scrappingV2.ipynb`, solo es cuestión de reproducirlo en el archivo `extractData.py` como fue para la gráfica de Ingresos Públicos.
Los pendientes son los siguientes:

- Gráfica de ingresos públicos.
    Falta obtener el deflactor real para calcular rate de FMP y de Otros impuestos.
- Gráfica de gasto programable. (La extracción de datos ya está). 
    Link de ejemplo https://ciep.mx/gasto-programable-informe-a-segundo-trimestre-abril-junio-2021/
- Gráfica del reporte de deuda. (La extracción de datos ya está). Link de ejemplo https://ciep.mx/reporte-de-deuda-segundo-trimestre-2021/
- Gráfica de gasto neto total. (La extracción de datos ya está). Link de ejemplo https://ciep.mx/gasto-neto-total-informe-a-segundo-trimestre-abril-junio-2021/